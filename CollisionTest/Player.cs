﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CollisionTest
{
    public class Player : GameObject
    {
        float movementSpeed;

        float fallSpeed;
        float fallSpeedDelta;

        float jumpSpeed;
        bool jumpTimedOut;
        Timer jumpTimer = new Timer();
        int jumpTimerDelay = 100;

        public Player (Transform transform) : base (transform)
        {
            base.Sprite = Game1.ContentManager.Load<Texture2D>("bar_hp");

            movementSpeed = 200f;
            fallSpeed = 1f;
            jumpSpeed = 400f;
            jumpTimedOut = false;
            jumpTimer.Interval = jumpTimerDelay;
            jumpTimer.Elapsed += jumpTimerHandler;
            jumpTimer.AutoReset = false;
        }

        public override void Update(GameTime gameTime)
        {

            if (Keyboard.GetState().IsKeyDown(Keys.A) || Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                this.Transform.DeltaPosition.X = -(float)(movementSpeed * gameTime.ElapsedGameTime.TotalSeconds);
            }

            //Right movement
            if (Keyboard.GetState().IsKeyDown(Keys.D) || Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                this.Transform.DeltaPosition.X = (float)(movementSpeed * gameTime.ElapsedGameTime.TotalSeconds);
            }



            if (Keyboard.GetState().IsKeyDown(Keys.Up) && fallSpeed == 0 && !jumpTimedOut)
            {
                this.fallSpeed = -(float)(jumpSpeed * gameTime.ElapsedGameTime.TotalSeconds);
                this.jumpTimedOut = true;
            }



            fallSpeedDelta = (float)(9.78f * gameTime.ElapsedGameTime.TotalSeconds);
            fallSpeed += fallSpeedDelta;

            this.Transform.DeltaPosition.Y += (int)fallSpeed;


        }

        public override void OnCollision(GameObject other)
        {
            Point myCenter = this.Hitbox.Center;
            float myHalfWidth = this.Hitbox.Width * 0.5f;
            float myHalfHeight = this.Hitbox.Height * 0.5f;

            float myLeftColX = myCenter.X - myHalfWidth;
            float myRightColX = myCenter.X + myHalfWidth;
            float myTopColY = myCenter.Y - myHalfHeight;
            float myBottomColY = myCenter.Y + myHalfHeight;



            Point otherCenter = other.Hitbox.Center;
            float otherHalfWidth = other.Hitbox.Width * 0.5f;
            float otherHalfHeight = other.Hitbox.Height * 0.5f;

            float otherLeftColX = otherCenter.X - otherHalfWidth;
            float otherRightColX = otherCenter.X + otherHalfWidth;
            float otherTopColY = otherCenter.Y - otherHalfHeight;
            float otherBottomColY = otherCenter.Y + otherHalfHeight;

            float dX = 0f;
            float dY = 0f;

            // Colliding my left on their right
            if (myLeftColX < otherRightColX && myRightColX > otherRightColX)
            {
                dX = otherRightColX - myLeftColX;

                if (this.fallSpeed < 0)
                {
                    this.fallSpeed = .1f;
                }

                Console.WriteLine();
            }
            // Colliding my Right on their left
            else if (myRightColX > otherLeftColX && myLeftColX < otherLeftColX)
            {
                dX = otherLeftColX - myRightColX;

                if (this.fallSpeed < 0)
                {
                    this.fallSpeed = .1f;
                }

                Console.WriteLine();
            }

            // Colliding with my top on their bottom
            if (myTopColY < otherBottomColY && myBottomColY > otherBottomColY)
            {
                dY = otherBottomColY - myTopColY;

            }
            // Colliding with my bottom on their top
            else if (myBottomColY > otherTopColY && myTopColY < otherTopColY)
            {
                dY = otherTopColY - myBottomColY;

                Console.WriteLine();
            }

            // If less collision on Y
            if (Math.Abs(dY) > Math.Abs(dX) && dX != 0 || Math.Abs(dX) > Math.Abs(dY) && dY == 0)
            {
                this.Transform.DeltaPosition.X += dX;
            }
            // if less collision on X or equal
            else if (Math.Abs(dX) > Math.Abs(dY) && dY != 0 || Math.Abs(dY) > Math.Abs(dX) && dX == 0)
            {
                this.Transform.DeltaPosition.Y += dY;

                // If colliding on bottom IE touching ground
                if (dY < 0)
                {
                    this.fallSpeed = 0f;

                    if (jumpTimedOut)
                    {
                        jumpTimer.Start();
                    }
                }
                else
                {
                    this.fallSpeed = .1f;
                }
            }

            Console.WriteLine();
        }

        private void jumpTimerHandler(object sender, EventArgs e)
        {
            jumpTimedOut = false;
        }
    }
}
