﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace CollisionTest
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<GameObject> gameObjects;

        private static ContentManager _content;
        /// <summary>
        /// ContentManager to load assets dynamically
        /// </summary>
        public static ContentManager ContentManager
        {
            get => _content;
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.PreferredBackBufferHeight = 480;
            graphics.PreferredBackBufferWidth = 600;

            Content.RootDirectory = "Content";
            IsFixedTimeStep = false;
            _content = Content;

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            gameObjects = new List<GameObject>();

            Player me = new Player(new Transform(new Vector2(300, 200), 0f));
            gameObjects.Add(me);

            //for (int i = 1; i < 5; i++)
            //{
            //    for (int j = 0; j < 6; j++)
            //    {
            //        gameObjects.Add(new GameObject("bar_mp", new Transform(new Vector2(j*150, i*100), 0f)));
            //    }
            //}

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    // For even
                    if (i%2 == 0)
                    {
                        gameObjects.Add(new GameObject("bar_mp", new Transform(new Vector2(j * 50, 70+(i * 100)), 0f)));
                    }
                    else
                    {
                        gameObjects.Add(new GameObject("bar_mp", new Transform(new Vector2(100+(j * 50), 70 + (i * 100)), 0f)));
                    }
                }
            }

            // Stik i javertusser
            gameObjects.Add(new GameObject("bar_mp", new Transform(new Vector2(10 * 50, 70 + (2 * 100) - 40), 0f)));
            gameObjects.Add(new GameObject("bar_mp", new Transform(new Vector2(50 + (0 * 50), 70 + (1 * 100)-40), 0f)));

            // Create bottom floor
            for (int i = 0; i < 12; i++)
            {
                gameObjects.Add(new GameObject("bar_mp", new Transform(new Vector2(i*50, 470), 0f)));
            }

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            foreach (GameObject obj in gameObjects)
            {
                obj.Update(gameTime);
            }

            foreach (GameObject obj in gameObjects)
            {
                foreach (GameObject other in gameObjects)
                {
                    if (obj != other && obj.CheckCollision(other))
                    {
                        obj.OnCollision(other);
                    }
                }
            }

            foreach (GameObject obj in gameObjects)
            {
                obj.PostUpdate(gameTime);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            // TODO: Add your drawing code here
            foreach (GameObject obj in gameObjects)
            {
                obj.Draw(spriteBatch);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
