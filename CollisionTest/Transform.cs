﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollisionTest
{
    public struct Transform
    {
        public Vector2 Position;
        public Vector2 DeltaPosition;
        public Vector2 Velocity;
        public float Rotation;

        public Transform(Vector2 pos, Vector2 v, float r)
        {
            this.Position = pos;
            this.Rotation = r;
            this.Velocity = v;
            this.DeltaPosition = Vector2.Zero;

        }

        public Transform(Vector2 pos, float r)
        {
            this.Position = pos;
            this.Rotation = r;
            this.Velocity = Vector2.Zero;
            this.DeltaPosition = Vector2.Zero;
        }
    }
}
