﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollisionTest
{
    public class GameObject
    {
        public Texture2D Sprite;
        public Transform Transform;

        public virtual Rectangle Hitbox
        {
            get { return new Rectangle((int)Transform.Position.X + (int)Transform.DeltaPosition.X, (int)Transform.Position.Y + (int)Transform.DeltaPosition.Y, Sprite.Width, Sprite.Height); }
        }

        public GameObject(string spriteName, Transform transform)
        {
            this.Transform = transform;
            Sprite = Game1.ContentManager.Load<Texture2D>(spriteName);
        }

        public GameObject(Transform transform)
        {
            this.Transform = transform;

        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void PostUpdate(GameTime gameTime)
        {
            Transform.Position += Transform.DeltaPosition;
            Transform.DeltaPosition = Vector2.Zero;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprite, Transform.Position, Color.Green);
        }

        public virtual bool CheckCollision(GameObject other)
        {
            return Hitbox.Intersects(other.Hitbox);
        }
        /// <summary>
        /// What to do on collision with other objects
        /// </summary>
        /// <param name="other">other objects</param>
        public virtual void OnCollision(GameObject other)
        {

        }

    }
}
